import de.pascalrost.tools.JSON.JSONArray;
import de.pascalrost.tools.JSON.JSONException;
import de.pascalrost.tools.JSON.JSONObject;
import de.pascalrost.tools.web.JSONObjectRequest;
import de.pascalrost.tools.web.StringRequest;
import de.pascalrost.tools.wpjson2java.enums.RequestMethod;


public class No2 {

	public static void main(String[] args) throws JSONException {
		// RUN IT ONE TIME WITH DEBUG
//		long startTime = System.currentTimeMillis();
//		run(true);
//		long stopTime = System.currentTimeMillis();
//	    long elapsedTime = stopTime - startTime;
//		System.out.println("-> Elapsed Time: " + elapsedTime + " ms");
		
		// RUN IT MULTIPLE TIMES WITHOUT DEBUG AND MEASURE TIME
		int loop = 100;
		System.out.println("Starting to loop Challange 2 for " + loop  + " times..");
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < loop; i++) {
			System.out.println("Loop No " + i);
			run(false);
		}
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("-> Elapsed Time: " + elapsedTime/1000 + " s");
	    System.out.println("-> Average Time for one pass: " + elapsedTime/loop + " ms");
	}
	
	public static boolean run(boolean debug) throws JSONException {
		//final boolean debug = true;
		
		String url = "http://84.200.109.239:5000/challenges/2/sorted/";
		String solutionUrl = "http://84.200.109.239:5000/solutions/2/";
		
		// DO JSON WEB REQUEST
		if(debug) {
			System.out.println("Getting Information from Challange Server..");
		}
		JSONObjectRequest jObjRequest = new JSONObjectRequest(url, RequestMethod.GET);
		JSONObject quest = jObjRequest.run();
		if(debug) {
			System.out.println(" -> done");
		}
		
		// INITIALIZE NEEDED VARIABLES
		int questSearchedInt = quest.getInt("k");
		JSONArray questList = quest.getJSONArray("list");
		
		// SEARCH FOR CORRECT VALUE
		if(debug) {
			System.out.println("Searching your number in the list.. Your number is: " + questSearchedInt + " // Length of List: " + questList.length());
		}
		int answerIndex = -1;
		for(int i = 0; i < questList.length(); i++) {
			if(questList.getInt(i) == questSearchedInt) {
				answerIndex = i; // YOU GOT THE SOLUTION!
				if(debug) {
					System.out.println(" -> Found your number at position " + answerIndex + "!");
				}
				
			}
		}
		
		// RESPOND WITH SOLUTION
		if(debug) {
			System.out.println("Sending your information to server..");
		}
		StringRequest solutionRequest = new StringRequest(solutionUrl, RequestMethod.POST);
		JSONObject solutionsRequestParams = new JSONObject();
		solutionsRequestParams.put("token", answerIndex);
		
		String finalResponse = solutionRequest.run(solutionsRequestParams);
		if(debug) {
			System.out.println(" -> done");
		}
		
		if(debug) {
			System.out.println(" -> Response: \n" + finalResponse);
		}
		
		
		if(finalResponse.equals("Success")) {
			return true;
		} else {
			return false;
		}
	}

}
